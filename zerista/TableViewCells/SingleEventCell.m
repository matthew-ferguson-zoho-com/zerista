//
//  SingleOnusCell.m
//  Daily Onus
//
//  Created by Matthew Ferguson on 3/15/16.
//  matthew.ferguson@zoho.com
//

#import "SingleEventCell.h"


@implementation SingleEventCell

@synthesize eventName, startTimeDate, finishTimeDate;


- (void)awakeFromNib {
    
    // Initialization code
    [super awakeFromNib];
    
    // Setup View
    [self setupView];
}




- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}



#pragma mark -
#pragma mark View Methods
- (void)setupView {


}


@end
