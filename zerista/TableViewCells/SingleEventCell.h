//
//  SingleOnusCell.h
//  Daily Onus
//
//  Created by Matthew Ferguson on 3/15/16.
//  matthew.ferguson@zoho.com
//

#import <UIKit/UIKit.h>


@interface SingleEventCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel * eventName;
@property (weak, nonatomic) IBOutlet UILabel * startTimeDate;
@property (weak, nonatomic) IBOutlet UILabel * finishTimeDate;


@end
