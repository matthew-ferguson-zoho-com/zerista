//
//  ListEventsVC.h
//  zerista
//
//  Created by Matthew Ferguson on 4/8/16.
//  Copyright © 2016 Matthew Ferguson. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>

#import "AppDelegate.h"

#import "SingleEventCell.h"

@interface ListEventsVC : UITableViewController 
{
    IBOutlet UITableView *tableView;
}

@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;
@property (strong, nonatomic) NSIndexPath *selection;


@end
