//
//  ListEventsVC.m
//  zerista
//
//  Created by Matthew Ferguson on 4/8/16.
//  Copyright © 2016 Matthew Ferguson. All rights reserved.
//

#import "ListEventsVC.h"

@interface ListEventsVC ()

@end

@implementation ListEventsVC

@synthesize tableView;


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    self.managedObjectContext = [appDelegate managedObjectContext];
    
    // Initialize Fetch Request
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"Events"];
    
    // Add Sort Descriptors
    [fetchRequest setSortDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:@"start" ascending:YES]]];
    
    // Initialize Fetched Results Controller
    self.fetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest managedObjectContext:self.managedObjectContext sectionNameKeyPath:nil cacheName:nil];
    
    // Configure Fetched Results Controller
    [self.fetchedResultsController setDelegate:(id)self];
    
    // Perform Fetch
    NSError *error = nil;
    [self.fetchedResultsController performFetch:&error];
    
    if (error) {
        NSLog(@"Unable to perform fetch.");
        NSLog(@"%@, %@", error, error.localizedDescription);
    }

    
    
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark -
#pragma mark Table View UITableViewDataSource


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView  {
    return [[self.fetchedResultsController sections] count];
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    NSArray *sections = [self.fetchedResultsController sections];
    id<NSFetchedResultsSectionInfo> sectionInfo = [sections objectAtIndex:section];
    return [sectionInfo numberOfObjects];
    
}



- (UITableViewCell *)tableView:(UITableView *)localTableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    SingleEventCell *cell = (SingleEventCell *)[localTableView dequeueReusableCellWithIdentifier:@"SingleEventCell" forIndexPath:indexPath];
    
    
    if (cell == nil){
        cell = [[SingleEventCell alloc] initWithStyle:UITableViewCellStyleDefault
                                        reuseIdentifier:@"SingleEventCell"];
    }

    // Configure Table View Cell
    [self configureCell:cell atIndexPath:indexPath];
    return cell;
}




- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}




- (void)tableView:(UITableView *)tableView
commitEditingStyle:(UITableViewCellEditingStyle)editingStyle
forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        
        NSManagedObject *record = [self.fetchedResultsController objectAtIndexPath:indexPath];
        if (record) {
            [self.fetchedResultsController.managedObjectContext deleteObject:record];
        }
        
    }
    
}






#pragma mark -
#pragma mark Table View Delegate Methods
- (void)tableView:(UITableView *)localTableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [localTableView deselectRowAtIndexPath:indexPath animated:YES];
    
}




- (void)configureCell:(SingleEventCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    
    
    NSManagedObject *record = [self.fetchedResultsController objectAtIndexPath:indexPath];
    
    
    [cell.eventName setText:[record valueForKey:@"name"]];
    
    //[cell.startTimeDate setText:[record valueForKey:@"start"]];
    [cell.startTimeDate setText:[self userVisibleDateTimeStringForRFC3339DateTimeString:[record valueForKey:@"start"]]];

    //[cell.finishTimeDate setText:[record valueForKey:@"finish"]];
     [cell.finishTimeDate setText:[self userVisibleDateTimeStringForRFC3339DateTimeString:[record valueForKey:@"start"]]];

}




- (void)controller:(NSFetchedResultsController *)controller
   didChangeObject:(id)anObject
       atIndexPath:(NSIndexPath *)indexPath
     forChangeType:(NSFetchedResultsChangeType)type
      newIndexPath:(NSIndexPath *)newIndexPath {
    
    
    switch (type) {
            
        case NSFetchedResultsChangeInsert: {
            
            [self.tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            
            break;
        }
            
        case NSFetchedResultsChangeDelete: {
            
            [self.tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
            
            break;
        }
            
        case NSFetchedResultsChangeUpdate: {
            
            [self configureCell:(SingleEventCell *)[self.tableView cellForRowAtIndexPath:indexPath] atIndexPath:indexPath];
            
            break;
        }
            
        case NSFetchedResultsChangeMove: {
            
            [self.tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
            
            [self.tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            
            break;
        }
            
    }
}




- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
}
      
      
#pragma mark - Utility Date,
- (NSString *)userVisibleDateTimeStringForRFC3339DateTimeString:(NSString *)rfc3339DateTimeString {
    
          /*
           Returns a user-visible date time string that corresponds to the specified
           RFC 3339 date time string. Note that this does not handle all possible
           RFC 3339 date time strings, just one of the most common styles.
           */
          
          NSDateFormatter *rfc3339DateFormatter = [[NSDateFormatter alloc] init];
          NSLocale *enUSPOSIXLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
          
          [rfc3339DateFormatter setLocale:enUSPOSIXLocale];
          [rfc3339DateFormatter setDateFormat:@"yyyy'-'MM'-'dd'T'HH':'mm':'ss'Z'"];
          [rfc3339DateFormatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
          
          // Convert the RFC 3339 date time string to an NSDate.
          NSDate *date = [rfc3339DateFormatter dateFromString:rfc3339DateTimeString];
          
          NSString *userVisibleDateTimeString;
          if (date != nil) {
              // Convert the date object to a user-visible date string.
              NSDateFormatter *userVisibleDateFormatter = [[NSDateFormatter alloc] init];
              assert(userVisibleDateFormatter != nil);
              
              [userVisibleDateFormatter setDateStyle:NSDateFormatterShortStyle];
              [userVisibleDateFormatter setTimeStyle:NSDateFormatterShortStyle];
              
              userVisibleDateTimeString = [userVisibleDateFormatter stringFromDate:date];
          }
          return userVisibleDateTimeString;
}
      


@end