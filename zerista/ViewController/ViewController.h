//
//  ViewController.h
//  zerista
//
//  Created by Matthew Ferguson on 4/8/16.
//  Copyright © 2016 Matthew Ferguson. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>

#import "AppDelegate.h"

@interface ViewController : UIViewController
{
    NSURLSession *session;
    __block NSURLSessionDataTask *task1;
}

@property (nonatomic, strong) __block NSURLSessionDataTask *task1;
@property (nonatomic, strong) NSURLSession * session;


@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;





-(void) sendREST_GetEvents:(NSString *) urlString;
-(void) cancelNetworkTasks;

- (void) persistEvents: (NSArray*) localEventsJsonArray;

- (NSString *)userVisibleDateTimeStringForRFC3339DateTimeString:(NSString *)rfc3339DateTimeString;

@end

