//
//  ViewController.m
//  zerista
//
//  Created by Matthew Ferguson on 4/8/16.
//  Copyright © 2016 Matthew Ferguson. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

@synthesize session;
@synthesize task1;


#pragma mark - View Controller Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.

    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    self.managedObjectContext = [appDelegate managedObjectContext];
}



- (void) viewDidAppear:(BOOL)animated {
    
    [self sendREST_GetEvents:@"https://zeristacon.zerista.com/event?format=json"];
    [task1 resume];//start the processing

}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}




#pragma mark - HTTP Errors

-(void) handleHttpErrors:(NSHTTPURLResponse *)httpResponse {
    
    /*
     400 Bad Request
     The server cannot or will not process the request due to something that is perceived to be a client error (e.g., malformed request syntax, invalid request message framing, or deceptive request routing).[14]
     */
    if([httpResponse statusCode] == 400) {
    }
    
    /*
     401 Unauthorized (RFC 7235)
     Similar to 403 Forbidden, but specifically for use when authentication is required and has failed or has not yet been provided. The response must include a WWW-Authenticate header field containing a challenge applicable to the requested resource. See Basic access authentication and Digest access authentication.
     */
    else if([httpResponse statusCode] == 401){
    }
    
    /*
     402 Payment Required
     Reserved for future use. The original intention was that this code might be used as part of some form of digital cash or micropayment scheme, but that has not happened, and this code is not usually used. YouTube uses this status if a particular IP address has made excessive requests, and requires the person to enter a CAPTCHA.[citation needed]
     */
    else if([httpResponse statusCode] == 402) {
    }
    
    /*
     403 Forbidden
     The request was a valid request, but the server is refusing to respond to it. Unlike a 401 Unauthorized response, authenticating will make no difference.
     */
    else if([httpResponse statusCode] == 403) {
    }
    
    /*
     404 Not Found
     The requested resource could not be found but may be available again in the future. Subsequent requests by the client are permissible.
     */
    else if([httpResponse statusCode] == 404) {
    }
    
    /*
     500 Internal Server Error
     A generic error message, given when an unexpected condition was encountered and no more specific message is suitable.
     */
    else if([httpResponse statusCode] == 500){
    }
    
    [self cancelNetworkTasks];
    
}



-(void) cancelNetworkTasks {
    
    if (task1.state == NSURLSessionTaskStateRunning ||
        task1.state == NSURLSessionTaskStateSuspended){
        [task1 cancel];
    }
    
    [self performSegueWithIdentifier:@"Network_Trouble_Segue" sender:self];
}

#pragma mark - REST GET Event Data

-(void) recvEventData: (NSData *)localData
{
    
    NSError *error = nil;
    NSArray *jsonArray;
    
    jsonArray = [NSJSONSerialization
                 JSONObjectWithData:localData
                 options: (NSJSONReadingAllowFragments | NSJSONReadingMutableContainers)
                 error: &error];
    
    if (!jsonArray){
        [self cancelNetworkTasks];
    }
    else {
        [self persistEvents:jsonArray];
        

        #if(0)
        for(NSDictionary *item in jsonArray) {
            NSString * startDateStr = [item objectForKey:@"start"];
            NSLog(@"%@", [self userVisibleDateTimeStringForRFC3339DateTimeString:startDateStr]);
        }
        #endif
        
        [self performSegueWithIdentifier:@"Events_Segue" sender:self];
    
    }
}



-(void) sendREST_GetEvents:(NSString *) urlString
{
    
    if([urlString length] <= 0)
    {
        // problems
        [self cancelNetworkTasks];
    }
    
    NSURL *url = [NSURL URLWithString:urlString];
    NSMutableURLRequest *requestUrl = [NSMutableURLRequest requestWithURL:url
                                                              cachePolicy:NSURLRequestReloadIgnoringCacheData
                                                          timeoutInterval:4.0];
    
    [requestUrl setValue:@"Mozilla/5.0" forHTTPHeaderField:@"User-Agent"];
    [requestUrl addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    requestUrl.HTTPMethod = @"GET";
    
    session = [NSURLSession sharedSession];
    task1 = [session dataTaskWithRequest:requestUrl
                       completionHandler: ^(NSData *rtndata, NSURLResponse *response, NSError *error)
             {
                 if (error != nil && error.code == NSURLErrorTimedOut)
                 {
                     [self cancelNetworkTasks];
                 }
                 else if (error != nil){
                     [self cancelNetworkTasks];
                 }
                 
                 NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                 
                 /*
                  200 OK
                  Standard response for successful HTTP requests. The actual response will depend on the request method used. In a GET request, the response will contain an entity corresponding to the requested resource. In a POST request, the response will contain an entity describing or containing the result of the action.
                  */
                 if([httpResponse statusCode] == 200)
                 {
                     if( ([rtndata length] > 0) && (error == nil) )
                     {
                         // now pass the data to a main threaded procedure, required by UIKit Responder chain,
                         // parse the json data, persist message data
                         
                         [self performSelectorOnMainThread:@selector(recvEventData:)
                                                withObject:rtndata waitUntilDone:NO];
                         
                     }
                     else if ([rtndata length] == 0 && error == nil) {
                         //[self performSelectorOnMainThread:@selector(emptyReply:) withObject:nil waitUntilDone:NO];
                         [self cancelNetworkTasks];
                     }
                 }
             }];
    
}





#pragma mark - Segue Support

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    // Make sure your segue name in storyboard is the same as this line
    if ([[segue identifier] isEqualToString:@"EventList_Segue"])
    {
        [segue.destinationViewController setTitle:@"List of Events"];
    }
    else if ([[segue identifier] isEqualToString:@"Network_Trouble_Segue"]) {
        // do nothing, but a message could be passed at this point.
    }
    
}


#pragma mark - Core Data Support

- (void) persistEvents: (NSArray*) localEventsJsonArray
{

    
    if ([localEventsJsonArray count] > 0)
    {
        
        
        // Create Entity
        NSEntityDescription *entity = [NSEntityDescription entityForName:@"Events" inManagedObjectContext:self.managedObjectContext];
        
        for(NSDictionary *anEvent in localEventsJsonArray)
        {
            
            // Initialize Record local scope event record
            NSManagedObject *record = [[NSManagedObject alloc] initWithEntity:entity insertIntoManagedObjectContext:self.managedObjectContext];
            
            #if(0)
            NSString * startDateStr = [anEvent objectForKey:@"start"];
            NSLog(@"%@", [self userVisibleDateTimeStringForRFC3339DateTimeString:startDateStr]);
            #endif
            
            // Populate event record
            [record setValue:[anEvent objectForKey:@"start"] forKey:@"start"];
            [record setValue:[anEvent objectForKey:@"finish"] forKey:@"finish"];
            [record setValue:[anEvent objectForKey:@"name"] forKey:@"name"];
            //NSLog(@"content == %@", [anEvent objectForKey:@"content"] ); NSLog(@" ");
            //[record setValue:[anEvent objectForKey:@"content"] forKey:@"content"];
            [record setValue:[anEvent objectForKey:@"icon_uri"] forKey:@"icon_uri"];
            
        }
        
        // Save Record
        NSError *error = nil;
        if ([self.managedObjectContext save:&error])
        {
            // Dismiss View Controller
            // BUZ [self dismissViewControllerAnimated:YES completion:nil];
        }
        else
        {
            
                if (error) {
                NSLog(@"Unable to save record.");
                NSLog(@"%@, %@", error, error.localizedDescription);
                }
            
                UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Warning" message:@"The JSON events WERE NOT persisted to Core Data. After Save" preferredStyle:UIAlertControllerStyleAlert];
            
                UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
                [alertController addAction:ok];
            
                [self presentViewController:alertController animated:YES completion:nil];
        
        }
        
        
    }
    else
    {
        
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Warning" message:@"The JSON events WERE NOT persisted to Core Data. Empty JSON Data Set" preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* ok =
            [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
        [alertController addAction:ok];
        
        [self presentViewController:alertController animated:YES completion:nil];

    }
    
    [self debugPrintRecord];
}



-(void) debugPrintRecord
{
    
    NSFetchRequest * fetchFirstRequest = [[NSFetchRequest alloc] initWithEntityName:@"Events"];
    
    // Add Sort Descriptor
    NSSortDescriptor *sortDescriptor1 = [NSSortDescriptor sortDescriptorWithKey:@"start" ascending:YES];
    [fetchFirstRequest setSortDescriptors:@[sortDescriptor1]];
    
    // Execute Fetch Request
    NSError *fetchError = nil;
    NSArray *resultFirstName = [self.managedObjectContext executeFetchRequest:fetchFirstRequest error:&fetchError];
    
    NSLog(@" "); NSLog(@"*************** BEGIN FETCH start finish name by start"); NSLog(@"  ");
    
    if (!fetchError) {
        for (NSManagedObject *managedObject in resultFirstName) {
            
            NSLog(@" ");
            NSLog(@"%@, %@, %@", [managedObject valueForKey:@"start"],
                  [managedObject valueForKey:@"finish"], [managedObject valueForKey:@"name"]);
            NSLog(@" ");
        }
    } else {
        NSLog(@"Error fetching data.");
        NSLog(@"%@, %@", fetchError, fetchError.localizedDescription);
    }
    
    NSLog(@"  "); NSLog(@"*************** END FETCH start finish name by start"); NSLog(@"  ");
    
}

    
@end
